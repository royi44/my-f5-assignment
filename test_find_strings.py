import urllib.request

URL_TO_CHECK = "https://the-internet.herokuapp.com/context_menu"
FAILED_TEST_MSG = "The string \"{0}\" was not found"
strings_to_find = [
    "Right-click in the box below to see one called \'the-internet\'",
    "Alibaba"
]

web_url = urllib.request.urlopen(URL_TO_CHECK)
html_data = web_url.read().decode(web_url.headers.get_content_charset())


def test_find_first_string():
    assert strings_to_find[0] in html_data, FAILED_TEST_MSG.format(strings_to_find[0])


def test_find_second_string():
    assert strings_to_find[1] in html_data, FAILED_TEST_MSG.format(strings_to_find[1])
